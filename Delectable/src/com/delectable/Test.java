package com.delectable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/test")
public class Test {
	
	@GET
	@Produces(MediaType.TEXT_XML)
	public String sayHello(){
		return "<?xml version='1.0'?>"
				+ "<hello>Hello World!</hello>";
	}

	
}
