package com.delectable;

public class MenuItem {

	private int id;
	String name;
	double pricePP;
	int minOrder;
	String []cat;
	
	public MenuItem () {
		id = IdGenerator.newIdMenu();
		name = "Pizza";
		pricePP = 10.0;
		minOrder = 5;
		cat = new String[3];
		cat[0]="Vegetarian";
	}
	
	public MenuItem(String n, double p, int m, String ...c) {
		name = n;
		pricePP = p;
		minOrder = m;
		cat = c;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getPricePP() {
		return pricePP;
	}

	public void setPricePP(double pricePP) {
		this.pricePP = pricePP;
	}

	public int getMinOrder() {
		return minOrder;
	}

	public void setMinOrder(int minOrder) {
		this.minOrder = minOrder;
	}

	public String[] getCat() {
		return cat;
	}

	public void setCat(String[] cat) {
		this.cat = cat;
	}

	public int getId() {
		return id;
	}	
}
