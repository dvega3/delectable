package com.delectable;
import java.util.concurrent.atomic.AtomicInteger;

public class IdGenerator {
	
	private static AtomicInteger menId = new AtomicInteger();
	
	public static int newIdMenu() {
		return menId.getAndIncrement();
	}

}
